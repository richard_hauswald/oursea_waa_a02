class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.text :body

      t.timestamps
    end
  end
end
CREATE TABLE comments
(
id INTEGER PRIMARY KEY NOT NULL,
                           post_id INTEGER,
                                   body TEXT,
                                        created_at TEXT,
                                                   updated_at TEXT
);
CREATE TABLE posts
(
id INTEGER PRIMARY KEY NOT NULL,
                           title TEXT,
                                 body TEXT,
                                      created_at TEXT,
                                                 updated_at TEXT
);
CREATE TABLE schema_migrations
(
version TEXT NOT NULL
);
CREATE TABLE sqlite_sequence
(
name TEXT,
     seq TEXT
);
CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations (version);
